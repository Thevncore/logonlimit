﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogonLimit
{
    public partial class ObstructForm : Form
    {
        Setting setting = Program.setting;
        DateTime now, enforceBegin, enforceEnd;
        DateTime lastUnlocked;
        TimeSpan allowance;

        bool unlocked = false;

        private void timer2_Tick(object sender, EventArgs e)
        {
            UpdateTime();

            if (unlocked)
                return;

            if (now >= enforceBegin && now <= enforceEnd)
            {
                timerHider.Enabled = true;
                Show();
            }
            else
            {
                unlocked = false; // reset unlocked status after out of enforce time
                Hide();
                timerHider.Enabled = false;                
            }
        }
        
        void UpdateTime()
        {
            now = DateTime.Now;
            enforceBegin = new DateTime(now.Year, now.Month, now.Day, setting.ThoiGianCamBatDau.Gio, setting.ThoiGianCamBatDau.Phut, 00);
            enforceEnd = new DateTime(now.Year, now.Month, now.Day + 1, setting.ThoiGianCamKetThuc.Gio, setting.ThoiGianCamKetThuc.Phut, 00);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == setting.MatKhauMoKhoa)
            {
                Hide();
                unlocked = true;
                timerHider.Enabled = false;
                lastUnlocked = DateTime.Now;
                timerLocker.Enabled = true;
            }
        }

        public ObstructForm()
        {
            InitializeComponent();
            allowance = new TimeSpan(setting.ThoiGianChoPhep.Gio, setting.ThoiGianChoPhep.Phut, 0);
        }

        private void ObstructForm_Load(object sender, EventArgs e)
        {
            timerChecker.Enabled = true;
            timer2_Tick(null, null);
        }

        private void timerLocker_Tick(object sender, EventArgs e)
        {
            DateTime reEnforce = lastUnlocked + allowance;
            if (DateTime.Now >= reEnforce)
            {
                timerLocker.Enabled = false;
                unlocked = false;
                Console.WriteLine("Relocked");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            WindowMinimize.MinimizeAll();
            this.BringToFront();
            this.Activate();
        }

        private void ObstructForm_Shown(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.ClearUndo();
        }
    }
}
