﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace LogonLimit
{
    static class Program
    {
        public static string Password = "";
        public static string DefaultSettingsString = @"{
    ""ThoiGianCamBatDau"":
    {
        ""Gio"":""23"",
        ""Phut"":""00""        
    },    
    ""ThoiGianCamKetThuc"":
    {
        ""Gio"":""03"",
        ""Phut"":""00""        
    },    
    ""ThoiGianChoPhep"":
    {
        ""Gio"":""01"",
        ""Phut"":""00""        
    },
    ""MatKhauMoKhoa"":""luatsu11110000""
}
";

        public static Setting setting = new Setting()
        {
            ThoiGianCamBatDau = new TimeDef() { Gio = 23, Phut = 00 },
            ThoiGianCamKetThuc = new TimeDef() { Gio = 6, Phut = 00 },
            ThoiGianChoPhep = new TimeDef() { Gio = 3, Phut = 00},
            MatKhauMoKhoa = "luatsu11110000"
        };

        static JavaScriptSerializer serializer = new JavaScriptSerializer();


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                var settingsString = File.ReadAllText(@"C:\users\luatsu\documents\settings.txt");
                setting = serializer.Deserialize<Setting>(settingsString);
            }
            catch
            {
                File.WriteAllText(@"C:\users\luatsu\documents\settings.txt", DefaultSettingsString);           
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ObstructForm());
        }
    }

    class Setting
    {
        public TimeDef ThoiGianCamBatDau;
        public TimeDef ThoiGianCamKetThuc;
        public TimeDef ThoiGianChoPhep;
        public string MatKhauMoKhoa;
    }

    struct TimeDef
    {
        public int Gio;
        public int Phut;
    }
}
